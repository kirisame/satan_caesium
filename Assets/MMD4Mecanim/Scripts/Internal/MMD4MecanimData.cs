﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

public static class MMD4MecanimData
{
	public enum FileType
	{
		None		= 0,
		PMD			= 1,
		PMX			= 2,
	}

	public enum MorphCategory
	{
		Base,
		EyeBrow,
		Eye,
		Lip,
		Other,
		Max,
	}

	public enum MorphType
	{
		Group,
		Vertex,
		Bone,
		UV,
		UVA1,
		UVA2,
		UVA3,
		UVA4,
		Material,
	}

	[System.Serializable]
	public class BoneData
	{
		public string						nameJp;
		public string						skeletonName;
		public uint							additionalFlags;

		public bool isRootBone				{ get { return (additionalFlags & 0xff000000u) == 0x80000000; } }
		//public bool isCharBone				{ get { return (additionalFlags & 0xff000000u) == 0xc0000000; } }
		//public bool isModLeftThumb0Bone		{ get { return (additionalFlags & 0xff000000u) == 0x08000000; } }
		//public bool isModRightThumb0Bone	{ get { return (additionalFlags & 0xff000000u) == 0x88000000; } }
		//public bool isModLeftShoulderBone	{ get { return (additionalFlags & 0xff000000u) == 0x48000000; } }
		//public bool isModRightShoulderBone	{ get { return (additionalFlags & 0xff000000u) == 0xc8000000; } }
	}

	public enum MorphMaterialOperation
	{
		Multiply,
		Adding,
	}

	public struct MorphMaterialData
	{
		public int							materialID;
		public MorphMaterialOperation		operation;
		public Color						diffuse;
		public Color						specular;
		public float						shininess;
		public Color						ambient;
		public Color						edgeColor;
		public float						edgeSize;
		public Color						textureColor;
		public Color						sphereColor;
		public Color						toonTextureColor;
	}

	[System.Serializable]
	public class MorphData
	{
		public string						nameJp;
		public MorphCategory				morphCategory;
		public MorphType					morphType;
		
		[NonSerialized]
		public bool							isMorphBaseVertex; // for PMD
		[NonSerialized]
		public int[]						indices;
		[NonSerialized]
		public float[]						weights;
		[NonSerialized]
		public Vector3[]					positions;
		[NonSerialized]
		public MorphMaterialData[]			materialData;
	}
	
	public class ModelData
	{
		public FileType						fileType;
		public int							vertexCount;
		public float						vertexScale;
		public float						importScale;
		public BoneData[]					boneDataList;
		public Dictionary< string, int >	boneDataDictionary;
		public MorphData[]					morphDataList;
		public Dictionary< string, int >	morphDataDictionary;

		public int GetMorphDataIndex( string morphName, bool isStartsWith )
		{
			if( morphName != null && this.morphDataList != null ) {
				if( morphDataDictionary != null ) {
					int morphIndex = 0;
					if( morphDataDictionary.TryGetValue( morphName, out morphIndex ) ) {
						return morphIndex;
					}
				}
				if( isStartsWith ) {
					for( int i = 0; i < this.morphDataList.Length; ++i ) {
						if( this.morphDataList[i].nameJp != null &&
						   this.morphDataList[i].nameJp.StartsWith( morphName ) ) {
							return i;
						}
					}
				}
			}

			return -1;
		}

		public MorphData GetMorphData( string morphName, bool isStartsWith )
		{
			int morphDataIndex = GetMorphDataIndex( morphName, isStartsWith );
			if( morphDataIndex != -1 ) {
				return this.morphDataList[morphDataIndex];
			}
			
			return null;
		}

		public MorphData GetMorphData( string morphName )
		{
			return GetMorphData( morphName, false );
		}
	}
	
	public class IndexData
	{
		public int[]						indexValues;
		
		public int vertexCount {
			get {
				if( this.indexValues != null && this.indexValues.Length > 0 ) {
					return this.indexValues[0];
				}

				return 0;
			}
		}
		
		public int meshCount {
			get {
				if( this.indexValues != null && this.indexValues.Length > 1 ) {
					return (int)((uint)this.indexValues[1] >> 24);
				}

				return 0;
			}
		}

		public int meshVertexCount {
			get {
				if( this.indexValues != null && this.indexValues.Length > 1 ) {
					return (int)((uint)this.indexValues[1] & 0x00ffffff);
				}

				return 0;
			}
		}
	}
	
	public class MorphMotionData
	{
		public string						name;
		public int[]						frameNos;
		public float[]						f_frameNos;
		public float[]						weights;
	}
	
	public class AnimData
	{
		public int							maxFrame;
		public MorphMotionData[]			morphMotionDataList;
	}
	
	public static ModelData BuildModelData( TextAsset modelFile )
	{
		if( modelFile == null ) {
			Debug.LogError( "BuildModelData: modelFile is norhing." );
			return null;
		}
		
		byte[] modelBytes = modelFile.bytes;
		
		if( modelBytes == null || modelBytes.Length == 0 ) {
			Debug.LogError( "BuildModelData: Nothing modelBytes." );
			return null;
		}
		
		MMD4MecanimCommon.BinaryReader binaryReader = new MMD4MecanimCommon.BinaryReader( modelBytes );
		if( !binaryReader.Preparse() ) {
			Debug.LogError( "BuildModelData:modelFile is unsupported fomart." );
			return null;
		}
		
		ModelData modelData = new ModelData();
		
		binaryReader.BeginHeader();
		modelData.fileType = (FileType)binaryReader.ReadHeaderInt(); // fileType
		binaryReader.ReadHeaderFloat(); // fileVersion
		binaryReader.ReadHeaderInt(); // fileVersion(BIN)
		binaryReader.ReadHeaderInt(); // additionalFlags
		modelData.vertexCount = binaryReader.ReadHeaderInt();
		binaryReader.ReadHeaderInt(); // vertexIndexCount
		modelData.vertexScale = binaryReader.ReadHeaderFloat(); // vertexScale
		modelData.importScale = binaryReader.ReadHeaderFloat(); // importScale
		binaryReader.EndHeader();
		
		int structListLength = binaryReader.structListLength;
		for( int structListIndex = 0; structListIndex < structListLength; ++structListIndex ) {
			if( !binaryReader.BeginStructList() ) {
				Debug.LogError("BuildModelData: Parse error.");
				return null;
			}
			int structFourCC = binaryReader.currentStructFourCC;
			if( structFourCC == MMD4MecanimCommon.BinaryReader.MakeFourCC( "BONE" ) ) {
				if( !_ParseBoneData( modelData, binaryReader ) ) {
					Debug.LogError("BuildModelData: Parse error.");
					return null;
				}
			} else if( structFourCC == MMD4MecanimCommon.BinaryReader.MakeFourCC( "MRPH" ) ) {
				if( !_ParseMorphData( modelData, binaryReader ) ) {
					Debug.LogError("BuildModelData: Parse error.");
					return null;
				}
			}
			if( !binaryReader.EndStructList() ) {
				Debug.LogError("BuildModelData: Parse error.");
				return null;
			}
		}		

		return modelData;
	}
	
	private static bool _ParseBoneData( ModelData modelData, MMD4MecanimCommon.BinaryReader binaryReader )
	{
		modelData.boneDataDictionary = new Dictionary<string, int>();
		modelData.boneDataList = new BoneData[binaryReader.currentStructLength];
		for( int structIndex = 0; structIndex < binaryReader.currentStructLength; ++structIndex ) {
			if( !binaryReader.BeginStruct() ) {
				return false;
			}
			
			BoneData boneData = new BoneData();
			
			boneData.additionalFlags = (uint)binaryReader.ReadStructInt();
			boneData.nameJp = binaryReader.GetName( binaryReader.ReadStructInt() );
			binaryReader.ReadStructInt();
			boneData.skeletonName = binaryReader.GetName( binaryReader.ReadStructInt() );
			binaryReader.ReadStructInt();
			binaryReader.ReadStructInt();
			binaryReader.ReadStructInt(); // orderedBoneID
			binaryReader.ReadStructInt(); // originalParentBoneID
			binaryReader.ReadStructInt(); // originalSortedBoneID
			binaryReader.ReadStructVector3(); // baseOriginAsLeftHand
			if( modelData.fileType == FileType.PMD ) {
				binaryReader.ReadStructInt();
				binaryReader.ReadStructInt();
				binaryReader.ReadStructInt();
				binaryReader.ReadStructFloat();
			} else if( modelData.fileType == FileType.PMX ) {
				binaryReader.ReadStructInt();
				binaryReader.ReadStructInt();
				binaryReader.ReadStructInt();
				binaryReader.ReadStructFloat();
				binaryReader.ReadStructInt();
			}

			if( !binaryReader.EndStruct() ) {
				return false;
			}
			
			modelData.boneDataList[structIndex] = boneData;
			if( !string.IsNullOrEmpty( boneData.skeletonName ) ) {
				modelData.boneDataDictionary[boneData.skeletonName] = structIndex;
			}
		}
		
		return true;
	}
	
	private static bool _ParseMorphData( ModelData modelData, MMD4MecanimCommon.BinaryReader binaryReader )
	{
		modelData.morphDataDictionary = new Dictionary<string, int>();
		modelData.morphDataList = new MorphData[binaryReader.currentStructLength];
		for( int structIndex = 0; structIndex < binaryReader.currentStructLength; ++structIndex ) {
			if( !binaryReader.BeginStruct() ) {
				return false;
			}
			
			MorphData morphData = new MorphData();
			
			int additionalFlags = binaryReader.ReadStructInt();
			int nameJp = binaryReader.ReadStructInt();
			binaryReader.ReadStructInt();
			int morphCategory = binaryReader.ReadStructInt();
			int morphType = binaryReader.ReadStructInt();
			int indexCount = binaryReader.ReadStructInt();
			
			morphData.nameJp		= binaryReader.GetName( nameJp );
			morphData.morphCategory	= (MorphCategory)morphCategory;
			morphData.morphType		= (MorphType)morphType;
			if( (additionalFlags & 0x01) != 0 ) {
				morphData.isMorphBaseVertex = true;
			}
			
			switch( morphType ) {
			case (int)MorphType.Vertex:
				morphData.indices = new int[indexCount];
				morphData.positions = new Vector3[indexCount];
				for( int i = 0; i < indexCount; ++i ) {
					morphData.indices[i] = binaryReader.ReadInt();
					morphData.positions[i] = binaryReader.ReadVector3();
					if( (uint)morphData.indices[i] >= modelData.vertexCount ) {
						Debug.LogError( "[" + structIndex + ":" + morphData.nameJp + "]:Invalid index. " + i + ":" + morphData.indices[i] );
						return false;
					}
				}
				break;
			case (int)MorphType.Group:
				morphData.indices = new int[indexCount];
				for( int i = 0; i < indexCount; ++i ) {
					morphData.indices[i] = binaryReader.ReadInt();
				}
				break;
			case (int)MorphType.Material:
				morphData.materialData = new MorphMaterialData[indexCount];
				for( int i = 0; i < indexCount; ++i ) {
					MorphMaterialData materialData = new MorphMaterialData();
					materialData.materialID = binaryReader.ReadInt();
					materialData.operation = (MorphMaterialOperation)binaryReader.ReadInt();
					materialData.diffuse = binaryReader.ReadColor();
					materialData.specular = binaryReader.ReadColorRGB();
					materialData.shininess = binaryReader.ReadFloat();
					materialData.ambient = binaryReader.ReadColorRGB();
					materialData.edgeColor = binaryReader.ReadColor();
					materialData.edgeSize = binaryReader.ReadFloat();
					materialData.textureColor = binaryReader.ReadColor();
					materialData.sphereColor = binaryReader.ReadColor();
					materialData.toonTextureColor = binaryReader.ReadColor();

					if( materialData.operation == MorphMaterialOperation.Adding ) {
						materialData.specular.a = 0;
						materialData.ambient.a = 0;
					}

					morphData.materialData[i] = materialData;
				}
				break;
			}

			if( !binaryReader.EndStruct() ) {
				return false;
			}
			
			modelData.morphDataList[structIndex] = morphData;
			if( !string.IsNullOrEmpty( morphData.nameJp ) ) {
				modelData.morphDataDictionary[morphData.nameJp] = structIndex;
			}
		}
		
		return true;
	}

	public static IndexData BuildIndexData( TextAsset indexFile )
	{
		if( indexFile == null ) {
			Debug.LogError( "BuildIndexData: indexFile is norhing." );
			return null;
		}
		
		byte[] indexBytes = indexFile.bytes;
		
		if( indexBytes == null || indexBytes.Length == 0 ) {
			Debug.LogError( "BuildIndexData: Nothing indexBytes." );
			return null;
		}
		int valueLength = indexBytes.Length / 4;
		int[] indexValues = new int[valueLength];
#if UNITY_WEBPLAYER
		try {
			using( MemoryStream memoryStraem = new MemoryStream( indexBytes ) ) {
				using( BinaryReader binaryReader = new BinaryReader( memoryStraem ) ) {
					for( int i = 0; i < valueLength; ++i )  {
						indexValues[i] = binaryReader.ReadInt32();
					}
				}
			}
		} catch( Exception ) {
			indexValues = null;
			return null;
		}
#else
		GCHandle gch = GCHandle.Alloc( indexBytes, GCHandleType.Pinned );
		Marshal.Copy( gch.AddrOfPinnedObject(), indexValues, 0, valueLength );
		gch.Free();
#endif
		if( indexValues.Length < 2 ) {
			Debug.LogError( "BuildIndexData:modelFile is unsupported fomart." );
			return null;
		}
		
		IndexData indexData = new IndexData();
		indexData.indexValues = indexValues;
		return indexData;
	}
	
	public static bool ValidateIndexData( IndexData indexData, SkinnedMeshRenderer[] skinnedMeshRenderers )
	{
		if( indexData == null || skinnedMeshRenderers == null ) {
			return false;
		}
		
		if( indexData.meshCount != skinnedMeshRenderers.Length ) {
			Debug.LogError( "ValidateIndexData: FBX reimported. Disabled morph, please recreate index file." );
			return false;
		} else {
			int meshVertexCount = 0;
			foreach( SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers ) {
				if( skinnedMeshRenderer.sharedMesh != null ) {
					meshVertexCount += skinnedMeshRenderer.sharedMesh.vertexCount;
				}
			}
			if( indexData.meshVertexCount != meshVertexCount ) {
				Debug.LogError( "ValidateIndexData: FBX reimported. Disabled morph, please recreate index file." );
				return false;
			}
		}
		
		return true;
	}

	public static AnimData BuildAnimData( TextAsset animFile )
	{
		if( animFile == null ) {
			Debug.LogError( "BuildAnimData: animFile is norhing." );
			return null;
		}
		
		byte[] animBytes = animFile.bytes;
		
		if( animBytes == null || animBytes.Length == 0 ) {
			Debug.LogError( "BuildAnimData: Nothing animBytes." );
			return null;
		}
		
		MMD4MecanimCommon.BinaryReader binaryReader = new MMD4MecanimCommon.BinaryReader( animBytes );
		if( !binaryReader.Preparse() ) {
			Debug.LogError( "BuildAnimData:animFile is unsupported fomart." );
			return null;
		}
		
		AnimData animData = new AnimData();
		
		binaryReader.BeginHeader();
		binaryReader.ReadHeaderInt(); // fileVersion(BIN)
		binaryReader.ReadHeaderInt(); // additionalFlags
		animData.maxFrame = binaryReader.ReadHeaderInt();
		binaryReader.EndHeader();
		
		int structListLength = binaryReader.structListLength;
		for( int structListIndex = 0; structListIndex < structListLength; ++structListIndex ) {
			if( !binaryReader.BeginStructList() ) {
				Debug.LogError("BuildAnimData: Parse error.");
				return null;
			}
			int structFourCC = binaryReader.currentStructFourCC;
			if( structFourCC == MMD4MecanimCommon.BinaryReader.MakeFourCC( "MRPH" ) ) {
				if( !_ParseMorphMotionData( animData, binaryReader ) ) {
					Debug.LogError("BuildAnimData: Parse error.");
					return null;
				}
			}
			if( !binaryReader.EndStructList() ) {
				Debug.LogError("BuildAnimData: Parse error.");
				return null;
			}
		}
		
		return animData;
	}

	private static bool _ParseMorphMotionData( AnimData animData, MMD4MecanimCommon.BinaryReader binaryReader )
	{
		animData.morphMotionDataList = new MMD4MecanimData.MorphMotionData[binaryReader.currentStructLength];
		for( int structIndex = 0; structIndex < binaryReader.currentStructLength; ++structIndex ) {
			if( !binaryReader.BeginStruct() ) {
				return false;
			}
			
			MorphMotionData morphMotionData = new MorphMotionData();
			binaryReader.ReadStructInt();
			morphMotionData.name = binaryReader.GetName( binaryReader.ReadStructInt() );
			int keyFrameLength = binaryReader.ReadStructInt();
			if( keyFrameLength < 0 ) {
				return false;
			}
			
			morphMotionData.frameNos = new int[keyFrameLength];
			morphMotionData.f_frameNos = new float[keyFrameLength];
			morphMotionData.weights = new float[keyFrameLength];
			
			for( int i = 0; i < keyFrameLength; ++i ) {
				morphMotionData.frameNos[i] = binaryReader.ReadInt();
				morphMotionData.weights[i] = binaryReader.ReadFloat();
				morphMotionData.f_frameNos[i] = (float)morphMotionData.frameNos[i];
			}
			
			animData.morphMotionDataList[structIndex] = morphMotionData;
			
			if( !binaryReader.EndStruct() ) {
				return false;
			}
		}
		
		return true;
	}
}
