#include "HLSLSupport.cginc"
#include "UnityShaderVariables.cginc"
#define UNITY_PASS_FORWARDBASE
#include "UnityCG.cginc"
#include "Lighting.cginc"
#include "MMD4Mecanim-MMDLit-AutoLight.cginc"

#define INTERNAL_DATA
#define WorldReflectionVector(data,normal) data.worldRefl
#define WorldNormalVector(data,normal) normal

#include "MMD4Mecanim-MMDLit-Surface-Lighting.cginc"

#ifdef LIGHTMAP_OFF
struct v2f_surf {
	float4 pos : SV_POSITION;
	float2 pack0 : TEXCOORD0;
	half3 normal : TEXCOORD1;
	half3 vlight : TEXCOORD2;
	half3 viewDir : TEXCOORD3;
	LIGHTING_COORDS(4,5)
	half2 mmd_uvSphere : TEXCOORD6;
	half3 mmd_tempDiffuse : TEXCOORD7;
};
#endif

#ifndef LIGHTMAP_OFF
struct v2f_surf {
	float4 pos : SV_POSITION;
	float2 pack0 : TEXCOORD0;
	half3 normal : TEXCOORD1;
	float2 lmap : TEXCOORD2;
#ifndef DIRLIGHTMAP_OFF
	half3 viewDir : TEXCOORD3;
	LIGHTING_COORDS(4,5)
	half2 mmd_uvSphere : TEXCOORD6;
	half3 mmd_tempDiffuse : TEXCOORD7;
#else
	LIGHTING_COORDS(3,4)
	half2 mmd_uvSphere : TEXCOORD5;
	half3 mmd_tempDiffuse : TEXCOORD6;
#endif
};
#endif

#ifndef LIGHTMAP_OFF
float4 unity_LightmapST;
#endif
float4 _MainTex_ST;

v2f_surf vert_surf(appdata_full v)
{
	v2f_surf o;
	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	o.pack0.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
	#ifndef LIGHTMAP_OFF
	o.lmap.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
	#endif
	float3 worldN = mul((float3x3)_Object2World, SCALED_NORMAL);
	o.normal = worldN;
	half3 norm = normalize(mul((float3x3)UNITY_MATRIX_MV, v.normal));
	half3 eye = normalize(mul(UNITY_MATRIX_MV, v.vertex).xyz);
	half3 r = reflect(eye, norm);
	half m = 2.0 * sqrt(r.x * r.x + r.y * r.y + (r.z + 1.0) * (r.z + 1.0));
	o.mmd_uvSphere.xy = r.xy / m + 0.5;
	o.mmd_tempDiffuse = MMDLit_GetTempDiffuse();
	#ifndef DIRLIGHTMAP_OFF
	TANGENT_SPACE_ROTATION;
	o.viewDir = (half3)mul(rotation, ObjSpaceViewDir(v.vertex));
	#else
	#ifdef LIGHTMAP_OFF
	o.viewDir = (half3)WorldSpaceViewDir(v.vertex);
	#endif
	#endif
	#ifdef LIGHTMAP_OFF
	o.vlight = ShadeSH9(float4(worldN, 1.0));
	#ifdef VERTEXLIGHT_ON
	float3 worldPos = mul(_Object2World, v.vertex).xyz;
	o.vlight += Shade4PointLights(
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, worldPos, worldN );
	#endif // VERTEXLIGHT_ON
	#endif // LIGHTMAP_OFF
	TRANSFER_VERTEX_TO_FRAGMENT(o);
	return o;
}

#ifndef LIGHTMAP_OFF
sampler2D unity_Lightmap;
#ifndef DIRLIGHTMAP_OFF
sampler2D unity_LightmapInd;
#endif
#endif

inline fixed4 frag_core(in v2f_surf IN, half3 albedo, half alpha)
{
	half atten = LIGHT_ATTENUATION(IN);
	half shadowAtten = SHADOW_ATTENUATION2(IN);
	half3 c = 0;

	#ifdef LIGHTMAP_OFF
	half NdotL = dot(IN.normal, _WorldSpaceLightPos0.xyz);
	half lambertStr = max(NdotL, 0.0);
	c = MMDLit_Lighting(
		albedo,
		IN.mmd_tempDiffuse,
		NdotL, lambertStr,
		IN.normal,
		_WorldSpaceLightPos0.xyz,
		normalize(IN.viewDir),
		atten,
		shadowAtten);
	#endif // LIGHTMAP_OFF || DIRLIGHTMAP_OFF
	#ifdef LIGHTMAP_OFF
	c += albedo * IN.vlight;
	#endif // LIGHTMAP_OFF

	#ifndef LIGHTMAP_OFF
	#ifndef DIRLIGHTMAP_OFF
	half3 specColor;
	half4 lmtex = tex2D(unity_Lightmap, IN.lmap.xy);
	half4 lmIndTex = tex2D(unity_LightmapInd, IN.lmap.xy);
	half3 lm = MMDLit_DirLightmap(
		IN.mmd_tempDiffuse,
		IN.normal,
		lmtex,
		lmIndTex,
		normalize(IN.viewDir),
		0,
		specColor);
	#else // !DIRLIGHTMAP_OFF
	half4 lmtex = tex2D(unity_Lightmap, IN.lmap.xy);
	half3 lm = MMDLit_Lightmap(
		IN.mmd_tempDiffuse,
		lmtex);
	#endif // !DIRLIGHTMAP_OFF
	atten = MMDLit_MulAtten(atten, shadowAtten);
	#ifdef SHADOWS_SCREEN
	#if (defined(SHADER_API_GLES) || defined(SHADER_API_GLES3)) && defined(SHADER_API_MOBILE)
	c = albedo * min(lm, atten*2);
	#else
	c = albedo * max(min(lm,(atten*2)*(half3)lmtex), lm*atten);
	#endif
	#else // SHADOWS_SCREEN
	c = albedo * lm;
	#endif // SHADOWS_SCREEN
	#ifndef DIRLIGHTMAP_OFF
	c += specColor;
	#else // !DIRLIGHTMAP_OFF
	#endif // !DIRLIGHTMAP_OFF
	#endif // LIGHTMAP_OFF

	return fixed4(c, alpha);
}

fixed4 frag_surf(v2f_surf IN) : COLOR
{
	half alpha;
	half3 albedo = MMDLit_GetAlbedo(IN.pack0.xy, IN.mmd_uvSphere, alpha);
	#if (defined(SHADER_API_GLES) && !defined(SHADER_API_GLES3)) && defined(SHADER_API_MOBILE)
	// Fix: GPU Adreno 205(OpenGL ES 2.0) discard crash
	#else
	clip(alpha - (1.1 / 255.0)); // Simulate MMD
	#endif
	
	return frag_core(IN, albedo, alpha);
}

fixed4 frag_fast(v2f_surf IN) : COLOR
{
	return frag_core(IN, MMDLit_GetAlbedo(IN.pack0.xy, IN.mmd_uvSphere), 1.0);
}
