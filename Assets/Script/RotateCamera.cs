﻿using UnityEngine;
using System.Collections;

public class RotateCamera : MonoBehaviour
{
    public GameObject Player;
    public float distance = -5.0f; //奥行き
    public float height = 1.0f; //高さ

    private float x = 0.0f;
    private float y = 0.0f;

    public float yMin = -20.0f;
    public float yMax = 80.0f;
    // Use this for initialization
    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.x;
        y = angles.y;

    }

    // Update is called once per frame
    void Update()
    {

        x += Input.GetAxis("Horizontal2") * 2.0f;
        y -= Input.GetAxis("Vertical2") * 2.0f;
        y = ClampAngle(y, yMin, yMax);

        Quaternion rotation = Quaternion.Euler(y, x, 0);
        Vector3 position = rotation * new Vector3(0.0f, height, distance) + Player.transform.position;
        transform.rotation = rotation;
        transform.position = position;

        transform.position = Quaternion.Euler(y, x, 0) * new Vector3(0.0f, height, distance) + Player.transform.position;

    }

    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}
